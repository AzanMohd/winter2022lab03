import java.util.*;

public class Shop{
	public static void main(String[] args){
		Laptop[] laptop = new Laptop[4];
		Scanner sc= new Scanner(System.in);
		
		for(int i = 0; i < laptop.length; i++){
			laptop[i] = new Laptop();
			
			System.out.println("Laptop "+(i+1));
			
			System.out.println("Enter a laptop brand:");
			laptop[i].brand = sc.nextLine();
			System.out.println("Enter the ammount of ram:");
			laptop[i].ram = Integer.parseInt(sc.nextLine());
			System.out.println("Enter the OS:");
			laptop[i].os = sc.nextLine();
			System.out.println("-----------------------------");
		}
		System.out.println("Here is the last product you chose:");
		System.out.println(laptop[3].brand);
		System.out.println(laptop[3].ram);
		System.out.println(laptop[3].os);
		laptop[3].brandIsAsus();
	}
}